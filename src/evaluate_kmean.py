from __future__ import print_function
import random

import numpy as np
from sklearn.externals import joblib

from dbconnection import MongoConnect
from decorator import timer
from recommender import RecommendSystem


def _get_dataset():
    bookzen = MongoConnect('mongodb://localhost:27017', 'bookzen')
    books = bookzen.process_item()

    def process_field(field="raw_description"):
        for book in books:
            yield book[field]

    dataset = process_field()
    bookzen.close_connection()
    return dataset


@timer
def main():
    text = ["bộ 7 cuốn mega luyện đề thpt quốc gia 2017 có những ưu điểm vượt trội 100 chuẩn cấu trúc đề thi 2017 theo những thay đổi mới nhất của bộ giáo dục và đào tạo megabook luôn cập nhật nhanh nhất kịp thời nhất cho ra đời những cuốn sách tối ưu nhất lộ trình chuẩn học theo năng lực giúp học sinh trung bình khá hay giỏi đều có thể ôn tập đạt điểm 8 9 thử sức với bộ 20 40 đề thi thử chuẩn cấu trúc thi 2017 miên phí 10 bộ đề thi thử online trên trang web megatest luyện thi giúp em thành thạo kĩ năng giải đề thpt quốc gia 2017 ghi nhớ kiến thức và rèn kĩ năng làm đề nhanh chính xác làm quen với áp lực thời gian của từng đề thi thi thử như thi thật rèn học sinh kĩ năng làm bài dưới áp lực thời gian lời giải định hướng tư duy phương pháp giải nhanh chỉ 1 phút cho 1 câu giúp em cân đối thời gian làm bài"]
    print(text)
    # dataset = _get_dataset()
    rs = RecommendSystem()
    vectorizer = rs.transform(text, load_model=True)

    file_dump = 'model/kmean_dump.pkl'
    km = joblib.load(file_dump)
    print(km.labels_.shape, km.labels_)

    idx_cluster = km.predict(vectorizer)
    # idx_cluster = km.fit_predict(vectorizer)
    print("predict: ", idx_cluster)
    ind_documents = list(np.where(km.labels_ == idx_cluster)[0])
    ind_documents = [int(num) for num in ind_documents]

    print("ind_documents: ", random.sample(ind_documents, 25))
    idx_random = random.sample(ind_documents, 25)
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.mongo_col.find({"ids": {"$in": idx_random}})
        for book in books:
            print('=' * 50)
            print(book["ids"], book["raw_description"][:250])


if __name__ == '__main__':
    main()
