from sklearn.externals import joblib

from dbconnection import MongoConnect
from decorator import timer
from recommender import RecommendSystem


def _get_dataset(field="raw_description"):
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.process_item()
        for book in books:
            yield book[field]


@timer
def main():
    dataset = _get_dataset()
    # print('==> Fit model')

    text = [
    "chắc hẳn bạn biết rất nhiều người cần thay đổi điều gì đó trong cuộc sống của họ nhưng dù bạn có cố gắng đến mấy có ý tốt đến mấy họ vẫn không chịu thay đổi bởi lẽ con người về cơ bản đều sợ thay đổi là một người kinh doanh một người cha một người bạn và cũng là một chuyên gia tư vấn rob jolles hiểu rất rõ viễn cảnh ấy từ kinh nghiệm kinh doanh thành công và nhiều năm nghiên cứu rob jolles đã đúc kết lại một công thức đơn giản giúp bạn dẫn dắt những người khác tự khám phá ra họ cần thay đổi những gì và vì sao có thể bạn muốn bán được hàng cũng có thể bạn đang muốn cải thiện một mối quan hệ dù có là gì thì những lời khuyên hữu ích của jolles cùng với những câu chuyện ví dụ thông minh cảm động và hài hước của anh sẽ giúp bạn hiểu rằng thay đổi suy nghĩ của một ai đó là một hành động xuất phát từ sự quan tâm và tình cảm cũng như từ niềm tin chắc chắn vào chính bản thân bạn mời bạn đón đọc"  # NOQA
    ]
    text = [
    "bộ 7 cuốn mega luyện đề thpt quốc gia 2017 có những ưu điểm vượt trội 100 chuẩn cấu trúc đề thi 2017 theo những thay đổi mới nhất của bộ giáo dục và đào tạo megabook luôn cập nhật nhanh nhất kịp thời nhất cho ra đời những cuốn sách tối ưu nhất lộ trình chuẩn học theo năng lực giúp học sinh trung bình khá hay giỏi đều có thể ôn tập đạt điểm 8 9 thử sức với bộ 20 40 đề thi thử chuẩn cấu trúc thi 2017 miên phí 10 bộ đề thi thử online trên trang web megatest luyện thi giúp em thành thạo kĩ năng giải đề thpt quốc gia 2017 ghi nhớ kiến thức và rèn kĩ năng làm đề nhanh chính xác làm quen với áp lực thời gian của từng đề thi thi thử như thi thật rèn học sinh kĩ năng làm bài dưới áp lực thời gian lời giải định hướng tư duy phương pháp giải nhanh chỉ 1 phút cho 1 câu giúp em cân đối thời gian làm bài"  # NOQA
    ]

    rs = RecommendSystem(use_cluster=False, use_stopword=False)
    rs.fit(dataset)

    # import os
    # dump_file = os.path.join(os.path.dirname(__file__), 'model/knn_dump.pkl')
    joblib.dump(rs, "model/knn_dump.pkl")
    del rs

    rs = joblib.load("model/knn_dump.pkl")
    vectorizer = rs.transform(text)
    ind_documents = rs.evaluate(vectorizer)
    ind_documents = [int(i) for i in ind_documents]
    # print(ind_documents)
    # print(text)
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.mongo_col.find({"ids": {"$in": ind_documents}})
        for book in books:
            # print('=' * 50)
            print(book["name"])


if __name__ == '__main__':
    main()
