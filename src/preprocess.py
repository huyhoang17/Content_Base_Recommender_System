import re
import string

from bson.objectid import ObjectId
from underthesea import word_sent

from dbconnection import MongoConnect


__all__ = ["preprocess"]

VN_accents = "aáàảãạăắằẳẵặâấầẩẫậđ₫eéèẻẽẹêếềểễệiíìỉĩị" \
             "oóòỏõọôốồổỗộơớờởỡợuúùủũụưứừửữựyýỳỷỹỵ"

WORDS = ''.join(
    set(VN_accents) | set(string.ascii_lowercase) | set(string.digits)
)

# punct_re = re.compile('[{}]'.format(re.escape(string.punctuation)))

punct_re = re.compile('[{}]'.format(
    re.escape(string.punctuation + string.whitespace)
))


def preprocess_vietnamese_accent(text):
    """
    Remove all specific charracters not in WORDS
    Save to `raw_description` field in model

    Parameters
    ----------
    text : str
    """
    text_preprocess = re.sub(
        "\s\s+", " ",
        re.sub('[^.{}]'.format(WORDS), ' ', text.lower()).strip()
    )

    return text_preprocess


def preprocess(text):
    """
    Remove puntuntion, multiple spaces

    Parameters
    ----------
    text : str
    """

    text = text or ""
    text = punct_re.sub(" ", text)  # remove punctuation
    text = re.sub("\s\s+", " ", text)  # remove multiple spaces
    text_preprocess = text.strip()

    return text_preprocess


def remove_space(text):
    text = re.sub("\s", "", text)
    return text


def process_raw_description(description):
    """
    Get `raw_description` (remove all specific characters)
    from each book's description

    Parameters
    ----------
    description : str, book's description
    """
    description = preprocess(description)
    raw_description = preprocess_vietnamese_accent(description)
    return raw_description


def preprocess_documents(update=False):
    """
    Process `raw description` for each document

    Parameters
    ----------
    update: bool, update new `raw_description`
    """
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.process_item()

        for book in books:
            if not preprocess(book["description"]):
                books.mongo_col.delete_one({"_id": ObjectId(book.get("_id"))})

            if book.get("raw_description") is None or update:
                raw_description = process_raw_description(book["description"])
                books.mongo_col.update(
                    {"_id": book["_id"]},
                    {"$set": {"raw_description": raw_description}}
                )


def process_vni_word_segmentation(update=False):

    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.process_item()

        for book in books:
            if not preprocess(book["description"]):
                books.mongo_col.delete_one({"_id": ObjectId(book.get("_id"))})

            vni_word_segmentation = word_sent(
                book["raw_description"], format="text"
            )
            try:
                books.mongo_col.update(
                    {"_id": book["_id"]},
                    {"$set": {"vni_word_segmentation": vni_word_segmentation}}
                )
            except ValueError:
                books.mongo_col.update(
                    {"_id": book["_id"]},
                    {"$set": {"vni_word_segmentation": ""}}
                )


def make_item_ids_to_query():

    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        count = 0
        books = db.process_item()
        for book in books:
            db.mongo_col.update(
                {"_id": book["_id"]},
                {"$set": {"ids": count}}
            )
            count += 1


if __name__ == '__main__':
    # make_item_ids_to_query()
    pass
