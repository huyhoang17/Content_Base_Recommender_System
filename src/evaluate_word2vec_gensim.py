import logging

from gensim.models import Word2Vec


logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO
)


def main():
    model = Word2Vec.load('model/100features_10context_7minwords.vec')
    # model = Word2Vec.load(
    #     'model/100features_10context_7minwords_perword.vec'
    # )

    document_sample = ["vị_trí", "thiết_kế",
                       "bách_khoa", "đại_học",
                       "ferguson"]
    document_sample = """
    mỗi câu_chuyện là một hành_trình như leo núi vô vùng gian_nan song đầy cảm_hứng của các doanh_nhân xã_hội bạn_đọc không chỉ biết về câu_chuyện khởi_nghiệp đơn_thuần của họ mà_cả những suy_tư hồn_nhiên ban_đầu những bước_đi hăm_hở đầu_tiên rồi những trăn_trở chơi hay bỏ khi lỗ cả năm trời cách họ đối_diện trước khó_khăn và thất_bại và cách họ gượng dậy bước tiếp với một quyết_tâm mạnh_mẽ hơn lúc_nào hết đến lúc này không_phải doanh_nghiệp của họ đã vượt qua mọi thách_thức nhưng một điều chắc_chắn là họ đã vượt qua chính mình câu_chuyện của họ vì_vậy là câu_chuyện của chính chúng_ta ai chẳng phải phấn_đấu vượt lên chính mình phạm kiều oanh giám_đốc người sáng_lập mời bạn đón đọc bách_khoa leading autobiography khởi_nghiệp ferguson
    """

    try:
        print(model.wv['cuốn'].shape)
        print(model.wv.most_similar(positive=['cuốn']))
        print(model.wv.similarity('cuốn', 'quyển'))
    except KeyError as e:
        print(e)

    if isinstance(document_sample, str):
        document_sample = document_sample.split()
    for word in document_sample:
        try:
            print("- {}: {}".format(
                word, model.wv.most_similar(positive=[word])[:5])
            )
        except KeyError as e:
            print(e)


if __name__ == '__main__':
    main()
