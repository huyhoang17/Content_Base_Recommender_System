from pymongo import TEXT

from dbconnection import MongoConnect
from decorator import timer
from preprocess import process_raw_description, remove_space

from fuzzywuzzy import fuzz

# n in n-grams model
N = 3


def ngrams(string, n=3):
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams
            if len(remove_space(''.join(ngram))) == n]


# ERROR
@timer
def create_index_for_search(db, index_name="search_port_ngrams",
                            override=False):
    print(db.mongo_col.index_information())
    try:
        if index_name not in db.mongo_col.index_information() or override:
            db.mongo_col.create_index(
                [
                    # TEXT == "text"
                    ("name", TEXT)
                    ("ngrams", TEXT),
                ],
                name=index_name,
                weights={
                    "ngrams": 100,
                    "name": 1,
                },
            )
    except Exception as e:
        print("ERROR: ", e)
    print(db.mongo_col.index_information())


def drop_indices(db, name):
    try:
        db.mongo_col.drop_index(name)
    except Exception as e:
        print(e)


@timer
def update_ngrams_for_search():
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.process_item()
        for book in books:
            name = process_raw_description(book["name"])
            db.mongo_col.update(
                {"_id": book["_id"]},
                {
                    "$set": {
                        "_id": book["_id"],
                        "ngrams": ' '.join(ngrams(name)),
                    },
                },
                # Perform an insert if no documents match the filter.
                upsert=False,
            )


@timer
def search_by_query(query, n_limit=25):
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        # query = ngrams(query)
        books = db.mongo_col.find(
            {
                "$text": {
                    "$search": query
                }
            },
            {
                # search only in (name, ngrams)
                # "name": True,
                # "ngrams": True,
                "score": {
                    "$meta": "textScore"
                }
            }
        ).sort([("score", {"$meta": "textScore"})]).limit(n_limit)

        for book in books:
            yield book["ids"]


def search_query_and_ngrams(query=None, n_limit=25):
    ids = []
    # if query is None:
    #     query = input("Query: ")
    books = search_by_query(query.lower())
    if books:
        ids.extend(books)

    queries = query.split()
    ids = []
    for i, q in enumerate(queries, start=1):
        if len(q) < N:
            # print("Query: %s" % q)
            books = search_by_query(q)
            if books:
                ids.extend(books)
        else:
            q = ngrams(q)
            for j in q:
                # print("Query: %s" % j)
                books = search_by_query(j.lower())
                if books:
                    ids.extend(books)

    ids = list(set(ids))
    # with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
    #     books = db.mongo_col.find({"ids": {"$in": ids}})
    #     for book in books[:n_limit]:
    #         print(book["name"])

    return ids


def process_keyword_query(ids, query, n_limit=None):
    """
    Get keyword search from query based on n-grams model
    Ex: feguso --> ferguson
    """
    keyword = None
    max_score = 0
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.mongo_col.find({"ids": {"$in": ids}})
        for book in books:
            name_split = book["name"].split()
            for word in name_split:
                score = fuzz.partial_ratio(word, query)
                if score > max_score:
                    max_score = score
                    keyword = word

    return keyword, max_score


@timer
def main():
    # with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
    #     create_index_for_search(db, "search_port_ngrams")
    query = "feguso"
    ids = search_query_and_ngrams(query)
    keyword, max_score = process_keyword_query(ids, query)
    print(keyword, max_score)
    search_query_and_ngrams(keyword)

    import logging
    from gensim.models import Word2Vec
    logging.basicConfig(
        format='%(asctime)s : %(levelname)s : %(message)s',
        level=logging.WARNING
    )

    keyword = keyword.lower()
    model = Word2Vec.load('model/100features_10context_7minwords.vec')
    try:
        most_similar = model.wv.most_similar(positive=[keyword])[:5]
        print("- {}: {}".format(keyword, most_similar))
    except KeyError as e:
        print(e)

    # for word, score in most_similar:
    #     books = search_by_query(word, 5)
    #     print([book for book in books])
    #     print("=" * 30)


if __name__ == '__main__':
    main()
