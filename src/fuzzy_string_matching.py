from sklearn.feature_extraction.text import TfidfVectorizer

from dbconnection import MongoConnect
from preprocess import process_raw_description


def ngrams(string, n=3):
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams]


def _get_dataset():
    bookzen = MongoConnect('mongodb://localhost:27017', 'bookzen')
    books = bookzen.process_item()

    def process_field(field="name"):
        for book in books:
            name = process_raw_description(book[field])
            yield name

    dataset = process_field()
    return dataset


if __name__ == "__main__":
    name = "Khiến Người Khác Thay Đổi Suy Nghĩ"
    vectorizer = TfidfVectorizer(
        max_df=0.5,
        # max_features=10000,
        min_df=2, use_idf=True,
        analyzer=ngrams
    )
    dataset = _get_dataset()
    # sparse matrix
    tf_idf_matrix = vectorizer.fit_transform(dataset)
    print(tf_idf_matrix.shape)
    tf_idf1 = tf_idf_matrix[0][tf_idf_matrix[0] != 0]
    print(tf_idf1.shape)
    for name, tf_idf in zip(ngrams(name), tf_idf1.reshape(-1, 1)):
        print(name, tf_idf)

    # document's name matching
    # DO NOT USE THIS ==> MemoryError
    # similarities = cosine_similarity(tf_idf_matrix)
    # print(similarities.shape)
