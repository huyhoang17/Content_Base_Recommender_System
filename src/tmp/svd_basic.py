import numpy as np
import scipy

from sklearn.feature_extraction.text import (
    TfidfVectorizer
)
from sklearn.externals import joblib
from sklearn.neighbors import NearestNeighbors

from dbconnection import MongoConnect


def _get_dataset(field="raw_description"):
    with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
        books = db.process_item()
        for book in books:
            yield book[field]


def _build_k_nearest_neighbors(poster_vect, n_recommend=20):
    if n_recommend is None:
        n_recommend = poster_vect.shape[0]
    rs_model = NearestNeighbors(
        n_neighbors=n_recommend,
        metric="cosine"
    ).fit(poster_vect)
    return rs_model


dataset = _get_dataset()

vectorizer = TfidfVectorizer(
    max_df=0.5, max_features=10000,
    min_df=2, use_idf=True
)

# tfidf_matrix = vectorizer.fit_transform(dataset)
# print(type(tfidf_matrix), tfidf_matrix.dtype,
#       tfidf_matrix.shape, tfidf_matrix.ndim)

# U, S, V = scipy.sparse.linalg.svds(tfidf_matrix, k=256)
# joblib.dump(vectorizer, "model/tfidf_model_basic.pkl")
# joblib.dump(U, "model/U_matrix.pkl")
# joblib.dump(S, "model/S_matrix.pkl")
# joblib.dump(V, "model/V_matrix.pkl")
# del U, S, V, vectorizer

text = [
"chắc hẳn bạn biết rất nhiều người cần thay đổi điều gì đó trong cuộc sống của họ nhưng dù bạn có cố gắng đến mấy có ý tốt đến mấy họ vẫn không chịu thay đổi bởi lẽ con người về cơ bản đều sợ thay đổi là một người kinh doanh một người cha một người bạn và cũng là một chuyên gia tư vấn rob jolles hiểu rất rõ viễn cảnh ấy từ kinh nghiệm kinh doanh thành công và nhiều năm nghiên cứu rob jolles đã đúc kết lại một công thức đơn giản giúp bạn dẫn dắt những người khác tự khám phá ra họ cần thay đổi những gì và vì sao có thể bạn muốn bán được hàng cũng có thể bạn đang muốn cải thiện một mối quan hệ dù có là gì thì những lời khuyên hữu ích của jolles cùng với những câu chuyện ví dụ thông minh cảm động và hài hước của anh sẽ giúp bạn hiểu rằng thay đổi suy nghĩ của một ai đó là một hành động xuất phát từ sự quan tâm và tình cảm cũng như từ niềm tin chắc chắn vào chính bản thân bạn mời bạn đón đọc"  # NOQA
]

tfidf_model = joblib.load("model/tfidf_model_basic.pkl")
U = joblib.load("model/U_matrix.pkl")
S = joblib.load("model/S_matrix.pkl")
V = joblib.load("model/V_matrix.pkl")
print(U.shape, S.shape, V.shape)
lsa_matrix = U.dot(np.diag(S))
print(type(lsa_matrix), lsa_matrix.dtype,
      lsa_matrix.shape, lsa_matrix.ndim)

rs_model = _build_k_nearest_neighbors(lsa_matrix)
joblib.dump(rs_model, "model/knn_dump_basic.pkl")
del rs_model

rs_model = joblib.load("model/knn_dump_basic.pkl")

vectorizer = tfidf_model.transform(text)
print(vectorizer.shape)
U_, S_, V_ = scipy.sparse.linalg.svds(vectorizer, k=1)

_, recommend_index = rs_model.kneighbors(U_.dot(np.diag(S_)))
ind_documents = [int(num) for num in list(recommend_index.flatten())]
print(ind_documents)
