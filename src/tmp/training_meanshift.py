from itertools import cycle


import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.datasets.samples_generator import make_blobs
from sklearn.datasets import load_iris
from sklearn.decomposition import PCA

# #############################################################################
# Generate sample data
# centers = [[1, 1], [-1, -1], [1, -1]]
# X, _ = make_blobs(n_samples=10000, centers=centers, cluster_std=0.6)
iris = load_iris()
X = iris.data
pca = PCA(n_components=2)
pca.fit(X)
X_train_pca = pca.transform(X)
print(X_train_pca)

# The following bandwidth can be automatically detected using
bandwidth = estimate_bandwidth(X_train_pca, quantile=0.2)

ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
ms.fit(X)
labels = ms.labels_
print(labels)
cluster_centers = ms.cluster_centers_
print(cluster_centers)

labels_unique = np.unique(labels)
n_clusters_ = len(labels_unique)
print(labels_unique, n_clusters_)

print("number of estimated clusters : %d" % n_clusters_)

# #############################################################################
# Plot result

plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
for k, col in zip(range(n_clusters_), colors):
    my_members = labels == k
    cluster_center = cluster_centers[k]
    plt.plot(X[my_members, 0], X[my_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()
