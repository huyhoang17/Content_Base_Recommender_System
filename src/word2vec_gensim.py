__doc__ = """
https://datascience.stackexchange.com/questions/678/what-are-some-standard-ways-of-computing-the-distance-between-documents

https://stats.stackexchange.com/questions/28406/is-cosine-similarity-a-classification-or-a-clustering-technique
"""

import logging

from gensim.models import word2vec

from dbconnection import MongoConnect


FMT_WORD2VEC = "model/{}features_{}context_{}minwords.vec"
FMT_WORD2VEC_PERWORDS = "model/{}features_{}context_{}minwords_perword.vec"


logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s', level=logging.WARNING
)


class MySentences(object):
    def __init__(self, field):
        self.field = field

    def __iter__(self):
        with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
            books = db.process_item()
            for book in books:
                yield book[self.field].split()
                # yield book["raw_description"].split()


def main():
    # sentences = []
    # with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
    #     books = db.process_item()
    #     for book in books:
    #         sentences.append(book["vni_word_segmentation"].split())

    sentences = MySentences("vni_word_segmentation")
    num_features = 100    # Word vector dimensionality
    min_word_count = 7    # Minimum word count
    num_workers = 4       # Number of threads to run in parallel
    context = 10          # Context window size
    downsampling = 1e-3   # Downsample setting for frequent words
    model = word2vec.Word2Vec(
        sentences, workers=num_workers,
        size=num_features, min_count=min_word_count,
        window=context, sample=downsampling, seed=1
    )
    fname = FMT_WORD2VEC.format(num_features, context, min_word_count)
    model.save(fname)
    del sentences


if __name__ == '__main__':
    main()
