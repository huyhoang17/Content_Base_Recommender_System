import json


def str_handler(string):
    if isinstance(string, str):
        return json.dumps(string)
    elif isinstance(string, unicode):
        return '''\"{0}\"'''.format(string.encode('utf-8'))
