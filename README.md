Bao cao
---

1. Tổng quan:
1.1: Hệ thống gợi ý là gì ?
1.2: Sự quan trọng của hệ gợi ý trong thời đại công nghệ số hiện nay
    1.2.1: Nhu cầu đặt ra 
    1.2.2: Mục tiêu cần đặt được
2. Các phương pháp gợi ý:
2.1: Hệ thống gợi ý dựa trên người dùng - lọc cộng tác (Collaborative filtering recommender systems)
    2.1.1: Mô hình toán học
    2.1.2: Cách thức hoạt động
    2.1.3: Ưu điểm
    2.1.4: Nhược điểm
2.2: Hệ thống gợi ý dựa trên nội dung (Content based recommender systems)
    2.2.1: Mô hình toán học
    2.2.2: Cách thức hoạt động
    2.2.3: Ưu điểm
    2.2.4: Nhược điểm 

3. Ứng dụng của hệ gợi ý trong thương mại điện tử hiện nay:
3.1: 1 vài tượng đài tiêu biểu
    3.1.1: Amazon, Last.fm - hệ gợi ý lọc cộng tác
    3.1.2: Pandora, Rotten Tomatoes - hệ gợi ý lọc nội dung
    3.1.3: Netflix: hệ gợi ý tích hợp

3.2: 1 số khó khăn khi xây dựng 1 hệ thống gợi ý phục vụ tốt nhu cầu người dùng
    3.2.1: Hiện tượng Long-Tail trong kinh doanh
    3.2.2: Khả năng mở rộng và đáp ứng nhu cầu của hệ thống
    3.2.3: Sự thiếu sót thông tin từ phía người dùng

3.3: Kết luận: